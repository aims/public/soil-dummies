import argparse
import os
import subprocess
import sys
import zipfile

import multidict
import requests

# generator = 'shell'
# semantic = True
# HWC = True

INTERFACES_PATH = os.path.join('..', 'assets', 'interfaces')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='SOIL dummy generator')
    parser.add_argument('filename', type=str)
    parser.add_argument('-g', '--generator', type=str, default='remote', choices=['shell', 'local', 'remote'])
    parser.add_argument('-s', '--semantic', action='store_true')
    parser.add_argument('-hwc', '--handwritten', action='store_true')

    args = parser.parse_args()

    soil_model = args.filename[:-5]

    if args.semantic:
        INTERFACES_PATH = os.path.join('..', 'assets', 'semantic_interfaces')

    # try:
    #     soil_model = sys.argv[1][:-5]
    #     if not os.path.exists(os.path.join(INTERFACES_PATH, f'{soil_model}.soil')):
    #         raise Exception()
    # except Exception as e:
    #     print('You must provide a file from the folder "interfaces" to be used as root file of the generation, e.g., "python generate.py lasertracker.soil".')
    #     exit()

    if args.generator in ['local', 'remote']:
        files = multidict.MultiDict()
        for filename in os.listdir(INTERFACES_PATH):
            files.add('files', (filename, open(os.path.join(INTERFACES_PATH, filename), 'r').read()))

        if args.handwritten:
            if os.path.exists(os.path.join('..', 'src', soil_model, 'hwc')):
                for filename in os.listdir(os.path.join('..', 'src', soil_model, 'hwc')):
                    if os.path.isfile(os.path.join('..', 'src', soil_model, 'hwc', filename)):
                        files.add('hwc',
                                  (filename,
                                   open(os.path.join('..', 'src', soil_model, 'hwc', filename), 'r').read()))
            # if os.path.exists(os.path.join('..', 'src', soil_model, 'device.py')):
            #     files.add('hwc', ('device.py', open(os.path.join('..', 'src', soil_model, 'device.py'), 'r').read()))

        url = f'http://localhost:8001/generate?model={soil_model}.soil&target=python'
        if args.generator == 'remote':
            url = f'https://iot.wzl-mq.rwth-aachen.de/soil-backend/generate?model={soil_model}.soil&target=python'

        if args.semantic:
            url = f'{url}&semantic=True'

        # # url = f'https://iot.wzl-mq.rwth-aachen.de/soil-backend/generate?model={soil_model}.soil&target=visual'
        # url = f'https://iot.wzl-mq.rwth-aachen.de/soil-backend/translateToVisual'
        # # url = f'http://localhost:8001/generate?model={soil_model}.soil&target=python'
        # # if HWC:
        # #     url = f'{url}'

        response = requests.post(url, files=files)
        if response.status_code == 200:
            if not os.path.exists(os.path.join('..', 'src', soil_model)):
                os.mkdir(os.path.join('..', 'src', soil_model))

            with open(os.path.join('..', 'src', soil_model, 'response.zip'), 'wb') as outfile:
                outfile.write(response.content)

            with zipfile.ZipFile(os.path.join('..', 'src', soil_model, 'response.zip'), 'r') as zip_ref:
                zip_ref.extractall(os.path.join('..', 'src', soil_model))

            os.remove(os.path.join('..', 'src', soil_model, 'response.zip'))
        else:
            print(response.status_code)

    elif args.generator == 'shell':
        command = f'java -jar ../../soil-text/target/libs/soil.jar soil.MainSoilTool -i {INTERFACES_PATH}/{soil_model}.soil -t python'
        if args.handwritten:
            command += f' -hwc ../src/{soil_model}/hwc'
        if args.semantic:
            command += ' -s'

        process = subprocess.Popen(command,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        print(stdout.decode())
        print(stderr.decode())
    else:
        print(f'Generator type "{args.generator}" is unknown, must be one of "shell", "local", "remote".')
