import json

import requests

print('GET')
response = requests.get('http://localhost:9000/')
print(response)
print(response.content)

data = {
    "class_name": "COMEnvironmentalSensor",
    "json_file": json.load(open("sensor.json", 'r')),
    "args": [],
    "kwargs": {'id': 'COM-Sensor2'}
}

print('PUT')
response = requests.put('http://localhost:9000/COM-SensorManager/COM-Sensor2', json.dumps(data))
print(response)
print(response.content)

# print('DELETE')
# response = requests.delete('http://localhost:9000/COM-SensorManager/COM-Sensor2')
# print(response)
# print(response.content)