from com_gripper import COMGripperTOP
from hwc.device import Device

class COMGripper(COMGripperTOP):

    def __init__(self, device: Device, open: bool = True):
        self._device = device
        self._par_open = open

    def get_par_open(self) -> bool:
        return self._par_open

    def set_par_open(self, open: bool):
        self._par_open = open
