import enum
import threading
import time

import numpy

from device import DeviceTOP


class Mode(enum.Enum):
    IDLE = 0
    AUTO = 1
    MANUAL = 2


class Device(DeviceTOP, threading.Thread):

    def __init__(self):
        DeviceTOP.__init__(self)
        threading.Thread.__init__(self)
        self.position = numpy.array([0, 0, 0], dtype=float)
        self.batterylevel = 100
        self.mode = Mode.AUTO
        self.target_position = self.position.copy()
        self.last_mode = Mode.IDLE

    def __del__(self):
        pass

    def goto(self, position: numpy.ndarray):
        self.last_mode = self.mode
        self.mode = Mode.MANUAL
        self.target_position = position.copy()

    def step(self, position: numpy.ndarray):
        self.last_mode = self.mode
        self.mode = Mode.MANUAL
        self.target_position = self.position + position

    def _load(self):
        while self.batterylevel < 100:
            self.batterylevel += 2
            time.sleep(0.01)

    def _reached_position(self):
        threshold = 0.01
        return all([abs(a - b) < threshold for a, b in zip(self.position, self.target_position)])

    def run(self) -> None:
        while True:
            if self.mode == Mode.AUTO:
                self.target_position = numpy.random.rand(3) * 100 - 50
                step = self.target_position - self.position
                while not self._reached_position():
                    if self.batterylevel > 5:
                        self.position += step / 100
                        self.batterylevel -= 1
                        time.sleep(0.1)
                    else:
                        self._load()
            elif self.mode == Mode.MANUAL:
                step = self.target_position - self.position
                while not self._reached_position():
                    if self.batterylevel > 5:
                        self.position += step / 100
                        self.batterylevel -= 1
                        time.sleep(0.1)
                    else:
                        self._load()
                tmp = self.mode
                self.mode = self.last_mode
                self.last_mode = tmp
