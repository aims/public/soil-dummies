from datetime import datetime

from base_stations.com_basestations import COMBaseStations
from hwc.device import Device
from mobile_entities.com_mobileentities import COMMobileEntities
from utils.enum_state import State

from com_lasertracker import COMLasertrackerTOP


class COMLasertracker(COMLasertrackerTOP):

    def __init__(self, device: Device, state: State = None, manufacturer: str = "", version: int = 0,
                 time: datetime = None):
        COMLasertrackerTOP.__init__(self, device, state, manufacturer, version, time)
        self._com_basestations = COMBaseStations(device)
        self._com_mobileentities = COMMobileEntities(device)

    def fun_reset(self):
        self._device.reset_tracker()

    def fun_shutdown(self):
        self._device.stop()

    def get_par_state(self):
        return str(self._device.state)

    def get_par_time(self):
        return datetime.now()
