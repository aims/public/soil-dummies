from hwc.device import Device
from mobile_entities.com_target import COMTargetTOP
from mobile_entities.enum_mode import Mode
from utils.enum_state import State


class COMTarget(COMTargetTOP):

    def __init__(self, device: Device, state: State = None, mode: Mode = None, type: str = ""):
        COMTargetTOP.__init__(self, device, state, mode, type)

    def fun_reset(self):
        self._device.reset_target()

    def fun_trigger(self, par_count=1, par_nonce=""):
        pass
        # for i in range(par_count):
        #     publish('OBJ-Lasertracker/OBJ-MobileEntities/OBJ-Home-Target/VAR-Position',
        #             json.dumps({'value': self.get_mea_position(), 'nonce': par_nonce}))
        #     time.sleep(0.1)

    def get_mea_position(self):
        return self._device.target.position

    def get_par_state(self):
        return str(self._device.target.state)

    def get_par_mode(self):
        return str(self._device.target.mode)
