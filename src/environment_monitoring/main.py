import toml

import start
from hwc.com_environmentalsensormanager import COMEnvironmentalSensorManager
from hwc.device import Device

if __name__ == '__main__':
    device = Device()
    manager = COMEnvironmentalSensorManager(device)

    start.start(manager, toml.load('config.toml'), 'model.json')