from com_environmentalsensor import COMEnvironmentalSensorTOP
from hwc.device import Device


class COMEnvironmentalSensor(COMEnvironmentalSensorTOP):

    def __init__(self, device: Device, location: str = "", id: str = ""):
        COMEnvironmentalSensorTOP.__init__(self, device, location)
        self.uuid = id
        
    def get_mea_temperature(self) -> float:
        return self._device.sensors[self.uuid].temperature

    def get_mea_pressure(self) -> float:
        return self._device.sensors[self.uuid].pressure

    def get_mea_humidity(self) -> float:
        return self._device.sensors[self.uuid].humidity

    def get_mea_signalstrength(self) -> int:
        return self._device.sensors[self.uuid].signal_strength

    def get_mea_batterylevel(self) -> int:
        return self._device.sensors[self.uuid].battery_level

    def get_par_location(self) -> str:
        return self._par_location

    def set_par_location(self, location: str):
        self._par_location = location
