import utils;

enum Mode {
    CONTINUOUS
    TRIGGERED
    EXTERNAL
    IDLE
}

parameter Mode {
    name: "Mode"
    description: "Current state of the entity. In CONTINUOUS mode, values are dispatched as fast as possible. In TRIGGERED mode, values are only dispatches after a software trigger. In EXTERNAL mode, values are dispatched in accordance to an external trigger, e.g. probe or TTL. IDLE means the entity is currently not used."
    default: CONTINUOUS
    datatype: enum
    range: Mode
    dimension: []
}

parameter Type {
    name: "Type"
    description: "System specific identifier of the target Type, e.g. SMR or Active SMR."
    datatype: string
    dimension: []
    default: "SMR"
}

measurement Locked {
    name: "Locked"
    description: "Boolean flag scpecifing whether the target is locked in."
    datatype: boolean
    dimension: []
}

function Reset {
    name: "Reset"
    description: "Starts the search routine around the current direction."
}

function Trigger {
    name: "Trigger"
    description: "Trigger count measurements and set the resulting label. This function is only allowed in triggered acquisition mode."
}

component Target {
    name: "Target"
    description: "Represents an individual mobile entity."
    functions:
        Reset reset
        Trigger trigger
    measurements:
        utils.Position position
        utils.Quaternion quaternion
        Locked locked
    parameters:
        utils.State state
        Mode mode
        Type type

    streams:
        position: update
        quaternion: update

    state.description = "Reflects the current state of the target. If logged in: OK. If not stable: WARNING. If lost: ERROR."
}

component MobileEntities {
    name: "Mobile Entities"
    description: "Object acting as a list of mobile entities in the metrology system."
    components:
        dynamic Target target
}